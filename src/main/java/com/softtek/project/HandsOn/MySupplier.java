package com.softtek.project.HandsOn;

@FunctionalInterface
public interface MySupplier<C extends Device> {
	 public C get (int id, String name, String desc, int manu, int color ,String comm);
}