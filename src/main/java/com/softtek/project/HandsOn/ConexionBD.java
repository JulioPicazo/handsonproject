package com.softtek.project.HandsOn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class ConexionBD {
	
	public void conex() {
		System.out.println("MySQL JDBC Connection");

        List<Device> result = new ArrayList<>();
        
        String SQL_SELECT = "Select * from device";
        
    	// auto close connection
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/prueba?serverTimezone=UTC#", "root", "1234")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

                ResultSet resultSet = preparedStatement.executeQuery();

                
                while (resultSet.next()) {
                	int id = resultSet.getInt("deviceId");
                	String name = resultSet.getString("name");
                	String desc = resultSet.getString("description");
                	int manu = resultSet.getInt("manufacturerId");
                	int color = resultSet.getInt("colorId");
                	String comm = resultSet.getString("comments");
 
                	MySupplier sup  = (i,n,c,b,m,j) -> new Device(i,n,c,b,m,j);
                	Device dao = sup.get(id,name,desc,manu,color,comm);
                	result.add(dao);
                	//result.forEach(System.out::println);
                }
              
            }
                else {
                System.out.println("Failed to make connection!");
            }
            
            System.out.println("--------------------1------------------------");
            BiConsumer<List<Device>,String> printValue = (l,v) -> {
            	for(Device col : l) {
            		switch(v) {
            		case "id":
            			 System.out.println(col.getColorId());
            		break;
            		case "name":
           			 System.out.println(col.getName());
           			break;
            		default:
           			 System.out.println(col.getDescription());
           		    break;
            		}
            	}
            };
            result.forEach(System.out::println);
            System.out.println("--------------------1------------------------");           
            System.out.println("--------------------2------------------------");
            
            List<String> DEV = result.stream()
                    .map(Device::getName)
                    .filter(h -> h.startsWith("L"))
                    .collect(Collectors.toList());
                    DEV.forEach(System.out::println);
            System.out.println("--------------------2------------------------");          
            System.out.println("--------------------3------------------------");
                        
            Integer a=0;
            System.out.println("Count the devices when ManufacturerId = 3" );
            		long DEVA=result.stream()
                    .map(Device::getManufacturerId)                 
                    .filter(COL->COL==3).count();	
            	System.out.println("There are: " + DEVA);
            	
            	 System.out.println("Count the devices when ColorID = 1" );
            	

                       
            System.out.println("--------------------3------------------------");
            System.out.println("--------------------4------------------------");
           
            Map<Integer, Device> mapa = result.stream().collect(Collectors.toMap(Device::getDeviceId,Function.identity()));
            mapa.forEach((k,v)->System.out.println("Key: "+ k +"value: "+ v.getName()));
       
            System.out.println("--------------------4------------------------");
            
            
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }   
	}
	 
}